package main;

import gui.GUIController;
import gui.GUIController.GUIControllerListener;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import db.DBController;

public class Main {
	public static String username;
	public static boolean isMember;
	private static GUIController guiController;
	private static DBController dbController;
	
	
	
	
	
	//initialize global username, member flag, GUI Controller, and Database Controller
	//create listeners for the controllers
	public static void main(String[] args) {
		username = null;
		isMember = false;
		guiController = new GUIController();
		try {
			dbController = new DBController();
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		guiController.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					dbController.close();
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
				guiController.dispose();
			}
		});
		guiController.setGUIControllerListener(new GUIControllerListener() {
			@Override
			public void loginPerformed(String username) {
				try {
					int flag = dbController.login(username);
					if(flag == DBController.MEMBER) {
						Main.username = username;
						isMember = true;
						guiController.showMemberScreen();
					}
					else if(flag == DBController.STAFF) {
						Main.username = username;
						isMember = false;
						guiController.showStaffScreen();
					}
					else
						JOptionPane.showMessageDialog(null, "Username not found.");
				} catch (HeadlessException | SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			@Override
			public void searchMoviePerformed(String title, String year, String genre, String actor, String director, String writer) {
				try {
					guiController.showResultsScreen(dbController.searchMovie(title, year, genre, actor, director, writer));
					guiController.setMemberSearchingShow(false);
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			@Override
			public void searchShowPerformed(String movie) {
				try {
					guiController.showResultsScreen(dbController.searchShow(movie));
					guiController.setMemberSearchingShow(isMember);
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			@Override
			public void deletePerformed(String title, String actor, String director, String writer) {
				try {
					dbController.removeInfo(title, actor, director, writer);
				} catch (HeadlessException | SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			@Override
			public void addPerformed(String title, String actor, String director, String writer) {
				try {
					dbController.addInfo(title, actor, director, writer);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			@Override
			public void buyTicketPerformed(String time, int room, String username) {
				try {
					dbController.buyTicket(time, room, username);
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		});
	}
}
