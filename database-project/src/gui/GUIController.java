package gui;

import gui.LoginScreen.LoginScreenListener;
import gui.MemberScreen.MemberScreenListener;
import gui.ResultsScreen.ResultsScreenListener;
import gui.StaffScreen.StaffScreenListener;
import java.awt.CardLayout;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JPanel;
import main.Main;

public class GUIController extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final String LOGIN_SCREEN = "login_screen", MEMBER_SCREEN = "member_screen";
	private static final String STAFF_SCREEN = "staff_screen", RESULTS_SCREEN = "results_screen";
	private JPanel screenHolder;
	private LoginScreen loginScreen;
	private MemberScreen memberScreen;
	private StaffScreen staffScreen;
	private ResultsScreen resultsScreen;
	private GUIControllerListener controllerListener;





	//initialize 4 different screens and a screen holder in the constructor
	//create listeners for the screens
	//use CardLayout to add all 4 screens
	//configure window properties
	public GUIController() {
		super("Database Application");

		screenHolder = new JPanel();
		loginScreen = new LoginScreen();
		memberScreen = new MemberScreen();
		staffScreen = new StaffScreen();
		resultsScreen = new ResultsScreen();

		loginScreen.setLoginScreenListener(new LoginScreenListener() {
			@Override
			public void loginPerformed(String username) {
				controllerListener.loginPerformed(username);
			}
		});
		memberScreen.setMemberScreenListener(new MemberScreenListener() {
			@Override
			public void searchMoviePerformed(String title, String year, String genre, String actor, String director, String writer) {
				controllerListener.searchMoviePerformed(title, year, genre, actor, director, writer);
			}
			@Override
			public void searchShowPerformed(String movie) {
				controllerListener.searchShowPerformed(movie);
			}
			@Override
			public void logOutPerformed() {
				showLoginScreen();
			}
		});
		staffScreen.setStaffScreenListener(new StaffScreenListener() {
			@Override
			public void logOutPerformed() {
				showLoginScreen();
			}
			@Override
			public void searchMoviePerformed(String title, String year, String genre, String actor, String director, String writer) {
				controllerListener.searchMoviePerformed(title, year, genre, actor, director, writer);
			}
			@Override
			public void searchShowPerformed(String movie) {
				controllerListener.searchShowPerformed(movie);
			}
			@Override
			public void addPerformed(String title, String actor, String director, String writer) {
				controllerListener.addPerformed(title, actor, director, writer);
			}
			@Override
			public void deletePerformed(String title, String actor, String director, String writer) {
				controllerListener.deletePerformed(title, actor, director, writer);
			}
		});
		resultsScreen.setResultsScreenListener(new ResultsScreenListener() {
			@Override
			public void backPerformed() {
				if(Main.isMember)
					showMemberScreen();
				else
					showStaffScreen();
			}
			@Override
			public void buyTicketPerformed(String time, int room, String username) {
				controllerListener.buyTicketPerformed(time, room, username);
			}
		});

		screenHolder.setLayout(new CardLayout());
		screenHolder.add(loginScreen, LOGIN_SCREEN);
		screenHolder.add(memberScreen, MEMBER_SCREEN);
		screenHolder.add(staffScreen, STAFF_SCREEN);
		screenHolder.add(resultsScreen, RESULTS_SCREEN);
		add(screenHolder);
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setSize(600, 500);
		setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/2 - getSize().width/2, Toolkit.getDefaultToolkit().getScreenSize().height/2 - getSize().height/2);
		setResizable(false);
		setVisible(true);
	}





	//switch to login screen
	public void showLoginScreen() {
		((CardLayout)screenHolder.getLayout()).show(screenHolder, LOGIN_SCREEN);
	}

	//switch to screen for members only
	public void showMemberScreen() {
		((CardLayout)screenHolder.getLayout()).show(screenHolder, MEMBER_SCREEN);
	}

	//switch to screen for staff only
	public void showStaffScreen() {
		((CardLayout)screenHolder.getLayout()).show(screenHolder, STAFF_SCREEN);
	}

	//switch to query results screen
	public void showResultsScreen(String results) {
		resultsScreen.setResults(results);
		((CardLayout)screenHolder.getLayout()).show(screenHolder, RESULTS_SCREEN);
	}





	//notify query results screen that a member searched for a show
	//this is needed so that the query results screen can toggle the buy ticket button on or off accordingly
	public void setMemberSearchingShow(boolean b) {
		resultsScreen.setMemberSearchingShow(b);
	}





	//set listener for this class
	public void setGUIControllerListener(GUIControllerListener controllerListener) {
		this.controllerListener = controllerListener;
	}

	//listener interface for this class
	public interface GUIControllerListener {
		public void loginPerformed(String username);
		public void buyTicketPerformed(String time, int room, String username);
		public void deletePerformed(String title, String actor, String director, String writer);
		public void addPerformed(String title, String actor, String director, String writer);
		public void searchMoviePerformed(String title, String year, String genre, String actor, String director, String writer);
		public void searchShowPerformed(String movie);
	}
}
