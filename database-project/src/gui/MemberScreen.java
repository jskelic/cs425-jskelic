package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

public class MemberScreen extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField actorField, directorField, writerField, yearField, movieField1, genreField, movieField2;
	private JButton logOutButton, searchMovieButton, searchShowButton;
	private MemberScreenListener screenListener;

	
	
	
	
	//initialize fields and buttons in the constructor
	//add listeners for the buttons
	//call configureLayout() helper method
	public MemberScreen() {
		actorField = new JTextField();
		directorField = new JTextField();
		writerField = new JTextField();
		yearField = new JTextField();
		movieField1 = new JTextField();
		genreField = new JTextField();
		movieField2 = new JTextField();
		logOutButton = new JButton("Log Out");
		searchMovieButton = new JButton("Search for Movie");
		searchShowButton = new JButton("Search for Show");

		logOutButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				screenListener.logOutPerformed();
			}
		});
		searchMovieButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String title = movieField1.getText().isEmpty()? null : movieField1.getText();
				String year = yearField.getText().isEmpty()? null : yearField.getText();
				String genre = genreField.getText().isEmpty()? null : genreField.getText();
				String actor = actorField.getText().isEmpty()? null : actorField.getText();
				String director = directorField.getText().isEmpty()? null : directorField.getText();
				String writer = writerField.getText().isEmpty()? null : writerField.getText();
				screenListener.searchMoviePerformed(title, year, genre, actor, director, writer);
			}
		});
		searchShowButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				screenListener.searchShowPerformed(movieField2.getText().isEmpty()? null : movieField2.getText());
			}
		});

		configureLayout();
	}

	//configure sizes
	//use GridBagLayout to lay components out in rows and columns
	private void configureLayout() {
		actorField.setPreferredSize(new Dimension(65, 25));
		directorField.setPreferredSize(new Dimension(65, 25));
		writerField.setPreferredSize(new Dimension(65, 25));
		yearField.setPreferredSize(new Dimension(65, 25));
		movieField1.setPreferredSize(new Dimension(65, 25));
		genreField.setPreferredSize(new Dimension(65, 25));
		movieField2.setPreferredSize(new Dimension(65, 25));
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.weightx = 1;
		c.insets = new Insets(0, 5, 0, 5);

		//first row///////////////////////////////////////////////////
		c.gridy = 0;
		c.weighty = 1;
		//column 8////////////////////////////
		c.gridx = 7;
		c.anchor = GridBagConstraints.LINE_START;
		add(logOutButton, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 2;
		add(new JSeparator(), c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 1;
		//column 1////////////////////////////
		c.gridx = 0;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("movie: "), c);
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		add(movieField1, c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("year: "), c);
		//column 4////////////////////////////
		c.gridx = 3;
		c.anchor = GridBagConstraints.LINE_START;
		add(yearField, c);
		//column 6////////////////////////////
		c.gridx = 5;
		c.anchor = GridBagConstraints.LINE_START;
		add(searchMovieButton, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		//column 1////////////////////////////
		c.gridx = 0;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("genre: "), c);
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		add(genreField, c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("actor: "), c);
		//column 4////////////////////////////
		c.gridx = 3;
		c.anchor = GridBagConstraints.LINE_START;
		add(actorField, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		//column 1////////////////////////////
		c.gridx = 0;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("director: "), c);
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		add(directorField, c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("writer: "), c);
		//column 4////////////////////////////
		c.gridx = 3;
		c.anchor = GridBagConstraints.LINE_START;
		add(writerField, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 2;
		add(new JSeparator(), c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 1;
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("movie: "), c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_START;
		add(movieField2, c);
		//column 6////////////////////////////
		c.gridx = 5;
		c.anchor = GridBagConstraints.LINE_START;
		add(searchShowButton, c);
	}
	
	
	
	
	
	//set listener for this class
	public void setMemberScreenListener(MemberScreenListener screenListener) {
		this.screenListener = screenListener;
	}

	//listener interface for this class
	public interface MemberScreenListener {
		public void searchMoviePerformed(String title, String year, String genre, String actor, String director, String writer);
		public void logOutPerformed();
		public void searchShowPerformed(String movie);
	}
}
