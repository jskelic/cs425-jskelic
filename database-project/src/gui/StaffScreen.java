package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

public class StaffScreen extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField actorField1, directorField1, writerField1, actorField2, directorField2, writerField2;
	private JTextField yearField, movieField1, genreField, movieField2, movieField3;
	private JButton logOutButton, searchMovieButton, searchShowButton, addButton, deleteButton;
	private StaffScreenListener screenListener;

	
	
	
	
	//initialize fields and buttons in the constructor
	//add listeners for the buttons
	//call configureLayout() helper method
	public StaffScreen() {
		actorField1 = new JTextField();
		directorField1 = new JTextField();
		writerField1 = new JTextField();
		actorField2 = new JTextField();
		directorField2 = new JTextField();
		writerField2 = new JTextField();
		yearField = new JTextField();
		movieField1 = new JTextField();
		genreField = new JTextField();
		movieField2 = new JTextField();
		movieField3 = new JTextField();
		logOutButton = new JButton("Log Out");
		searchMovieButton = new JButton("Search for Movie");
		searchShowButton = new JButton("Search for Show");
		addButton = new JButton("Add");
		deleteButton = new JButton("Delete");

		logOutButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				screenListener.logOutPerformed();
			}
		});
		searchMovieButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String title = movieField1.getText().isEmpty()? null : movieField1.getText();
				String year = yearField.getText().isEmpty()? null : yearField.getText();
				String genre = genreField.getText().isEmpty()? null : genreField.getText();
				String actor = actorField1.getText().isEmpty()? null : actorField1.getText();
				String director = directorField1.getText().isEmpty()? null : directorField1.getText();
				String writer = writerField1.getText().isEmpty()? null : writerField1.getText();
				screenListener.searchMoviePerformed(title, year, genre, actor, director, writer);
			}
		});
		searchShowButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				screenListener.searchShowPerformed(movieField2.getText().isEmpty()? null : movieField2.getText());
			}
		});
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String title = movieField3.getText().isEmpty()? null : movieField3.getText();
				String actor = actorField2.getText().isEmpty()? null : actorField2.getText();
				String director = directorField2.getText().isEmpty()? null : directorField2.getText();
				String writer = writerField2.getText().isEmpty()? null : writerField2.getText();
				screenListener.addPerformed(title, actor, director, writer);
			}
		});
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String title = movieField3.getText().isEmpty()? null : movieField3.getText();
				String actor = actorField2.getText().isEmpty()? null : actorField2.getText();
				String director = directorField2.getText().isEmpty()? null : directorField2.getText();
				String writer = writerField2.getText().isEmpty()? null : writerField2.getText();
				screenListener.deletePerformed(title, actor, director, writer);
			}
		});

		configureLayout();
	}

	//configure sizes
	//use GridBagLayout to lay components out in rows and columns
	private void configureLayout() {
		actorField1.setPreferredSize(new Dimension(65, 25));
		directorField1.setPreferredSize(new Dimension(65, 25));
		writerField1.setPreferredSize(new Dimension(65, 25));
		actorField2.setPreferredSize(new Dimension(65, 25));
		directorField2.setPreferredSize(new Dimension(65, 25));
		writerField2.setPreferredSize(new Dimension(65, 25));
		yearField.setPreferredSize(new Dimension(65, 25));
		movieField1.setPreferredSize(new Dimension(65, 25));
		genreField.setPreferredSize(new Dimension(65, 25));
		movieField2.setPreferredSize(new Dimension(65, 25));
		movieField3.setPreferredSize(new Dimension(65, 25));
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.weightx = 1;
		c.insets = new Insets(0, 5, 0, 5);

		//first row///////////////////////////////////////////////////
		c.gridy = 0;
		c.weighty = 1;
		//column 8////////////////////////////
		c.gridx = 7;
		c.anchor = GridBagConstraints.LINE_START;
		add(logOutButton, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 2;
		add(new JSeparator(), c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 1;
		//column 1////////////////////////////
		c.gridx = 0;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("movie: "), c);
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		add(movieField1, c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("year: "), c);
		//column 4////////////////////////////
		c.gridx = 3;
		c.anchor = GridBagConstraints.LINE_START;
		add(yearField, c);
		//column 6////////////////////////////
		c.gridx = 5;
		c.anchor = GridBagConstraints.LINE_START;
		add(searchMovieButton, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		//column 1////////////////////////////
		c.gridx = 0;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("genre: "), c);
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		add(genreField, c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("actor: "), c);
		//column 4////////////////////////////
		c.gridx = 3;
		c.anchor = GridBagConstraints.LINE_START;
		add(actorField1, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		//column 1////////////////////////////
		c.gridx = 0;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("director: "), c);
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		add(directorField1, c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("writer: "), c);
		//column 4////////////////////////////
		c.gridx = 3;
		c.anchor = GridBagConstraints.LINE_START;
		add(writerField1, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 2;
		add(new JSeparator(), c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 1;
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("movie: "), c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_START;
		add(movieField2, c);
		//column 6////////////////////////////
		c.gridx = 5;
		c.anchor = GridBagConstraints.LINE_START;
		add(searchShowButton, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 2;
		add(new JSeparator(), c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		c.weighty = 1;
		//column 1////////////////////////////
		c.gridx = 0;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("movie: "), c);
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		add(movieField3, c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("actor: "), c);
		//column 4////////////////////////////
		c.gridx = 3;
		c.anchor = GridBagConstraints.LINE_START;
		add(actorField2, c);
		//column 6////////////////////////////
		c.gridx = 5;
		c.anchor = GridBagConstraints.LINE_START;
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(addButton);
		panel.add(deleteButton);
		add(panel, c);

		//next row///////////////////////////////////////////////////
		c.gridy++;
		//column 1////////////////////////////
		c.gridx = 0;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("director: "), c);
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		add(directorField2, c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("writer: "), c);
		//column 4////////////////////////////
		c.gridx = 3;
		c.anchor = GridBagConstraints.LINE_START;
		add(writerField2, c);
	}

	
	
	
	
	//set listener for this class
	public void setStaffScreenListener(StaffScreenListener screenListener) {
		this.screenListener = screenListener;
	}
	
	//listener interface for this class
	public interface StaffScreenListener {
		public void logOutPerformed();
		public void deletePerformed(String title, String actor, String director, String writer);
		public void addPerformed(String title, String actor, String director, String writer);
		public void searchShowPerformed(String movie);
		public void searchMoviePerformed(String title, String year, String genre, String actor, String director, String writer);
	}
}
