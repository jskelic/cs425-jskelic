package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LoginScreen extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField usernameField;
	private JButton loginButton;
	private LoginScreenListener screenListener;

	
	
	
	
	//initialize fields and buttons in the constructor
	//add listeners for the buttons
	//call configureLayout() helper method
	public LoginScreen() {
		usernameField = new JTextField();
		loginButton = new JButton("Login");

		loginButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				screenListener.loginPerformed(usernameField.getText());
			}
		});

		configureLayout();
	}

	//configure sizes
	//use GridBagLayout to lay components out in rows and columns
	private void configureLayout() {
		usernameField.setPreferredSize(new Dimension(65, 25));
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.weightx = 1;
		c.insets = new Insets(0, 15, 0, 15);

		//first row///////////////////////////////////////////////////
		c.gridy = 0;
		c.weighty = 1;
		//column 1////////////////////////////
		c.gridx = 0;
		c.anchor = GridBagConstraints.LINE_END;
		add(new JLabel("Username: "), c);
		//column 2////////////////////////////
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		add(usernameField, c);
		//column 3////////////////////////////
		c.gridx = 2;
		c.anchor = GridBagConstraints.LINE_START;
		add(loginButton, c);
	}
	
	
	
	
	
	//set listener for this class
	public void setLoginScreenListener(LoginScreenListener screenListener) {
		this.screenListener = screenListener;
	}

	//listener interface for this class
	public interface LoginScreenListener {
		public void loginPerformed(String username);
	}
}
