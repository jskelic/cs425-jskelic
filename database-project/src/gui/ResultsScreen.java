package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import main.Main;

public class ResultsScreen extends JPanel {
	private static final long serialVersionUID = 1L;
	JTextArea textArea;
	JTextField textField;
	JLabel label;
	JButton backButton, buyTicketButton;
	ResultsScreenListener screenListener;





	//initialize fields and buttons in the constructor
	//add listeners for the buttons
	//configure sizes and font
	//use FlowLayout to lay components out linearly
	public ResultsScreen() {
		textArea = new JTextArea();
		textField = new JTextField();
		label = new JLabel("Line number:");
		backButton = new JButton("Back");
		buyTicketButton = new JButton("Buy Ticket");

		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				screenListener.backPerformed();
			}
		});
		buyTicketButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Scanner scanner = new Scanner(textArea.getText());
				int count = -1;
				try {
					while(scanner.hasNextLine() && count++ < Integer.parseInt(textField.getText()))
						scanner.nextLine();
					StringTokenizer tokenizer = new StringTokenizer(scanner.nextLine(), "\t");
					scanner.close();
					tokenizer.nextToken();
					tokenizer.nextToken();
					screenListener.buyTicketPerformed(tokenizer.nextToken().trim(), Integer.parseInt(tokenizer.nextToken().trim()), Main.username);
				} catch (NumberFormatException exception) {
					JOptionPane.showMessageDialog(null, "Enter a positive number.");
				} catch (NoSuchElementException exception) {
					JOptionPane.showMessageDialog(null, "Enter a valid line number.");
				}
			}
		});

		textField.setPreferredSize(new Dimension(35, 25));
		textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));

		setLayout(new FlowLayout());
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setPreferredSize(new Dimension(500, 400));
		add(scrollPane);
		add(backButton);
		add(label);
		add(textField);
		add(buyTicketButton);
	}





	//if a member is searching for a show this screen will show the buy ticket button and associated components
	//otherwise the buying ticket functionality is hidden
	public void setMemberSearchingShow(boolean b) {
		label.setVisible(b);
		textField.setVisible(b);
		buyTicketButton.setVisible(b);
	}





	//show the query results in a text area
	public void setResults(String results) {
		textArea.setText(results);
	}





	//set listener for this class
	public void setResultsScreenListener(ResultsScreenListener screenListener) {
		this.screenListener = screenListener;
	}

	//listener interface for this class
	public interface ResultsScreenListener {
		public void backPerformed();
		public void buyTicketPerformed(String time, int room, String username);
	}
}
