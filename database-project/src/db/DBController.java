package db;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import javax.swing.JOptionPane;

public class DBController {
	public static final int MEMBER = 0, STAFF = 1;
	private Connection conn;





	//connect to database in the constructor
	public DBController() throws ClassNotFoundException, SQLException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		conn = DriverManager.getConnection("jdbc:oracle:thin:@fourier.cs.iit.edu:1521:orcl", "jskelic", "ora15786d"); 
	}

	//disconnect from database
	public void close() throws SQLException {
		conn.close();
	}





	//query the users table with username parameter and return whether the user is a member or staff, or -1 if the username is not in users
	public int login(String username) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT cinema_member, staff FROM users WHERE username = ?");
		statement.setString(1, username);
		ResultSet resultSet = statement.executeQuery();
		int flag;
		if(resultSet.next())
			flag = resultSet.getInt(1) == 1? MEMBER : STAFF;
		else
			flag = -1;
		resultSet.close();
		statement.close();
		return flag;
	}





	//query a table formed from joins between movie, movie_genre, acts, directs, and writes using parameters 
	//return a formatted string of the result of the query
	public String searchMovie(String title, String year, String genre, String actor, String director, String writer) throws SQLException {
		String query = "SELECT title, movie_year year, length_minutes length, pg_rating, genre, acts.pName actor, acts.movie_role actor_role, directs.pName director, writes.pName writer " +
				"FROM (movie NATURAL FULL JOIN movie_genre) NATURAL FULL JOIN ((acts FULL JOIN directs USING (title,movie_year)) FULL JOIN writes USING (title,movie_year)) WHERE 1 = 1";
		LinkedList<String> queue = new LinkedList<>();
		if(title != null) {
			query += " AND title = ?";
			queue.addLast(title);
		}
		if(year != null) {
			query += " AND movie_year = ?";
			queue.addLast(year);
		}
		if(genre != null) {
			query += " AND genre = ?";
			queue.addLast(genre);
		}
		if(actor != null) {
			query += " AND acts.pName = ?";
			queue.addLast(actor);	
		}
		if(director != null) {
			query += " AND directs.pName = ?";
			queue.addLast(director);	
		}
		if(writer != null) {
			query += " AND writes.pName = ?";
			queue.addLast(writer);
		}
		PreparedStatement statement = conn.prepareStatement(query);
		int iter = 1;
		while(!queue.isEmpty())
			statement.setString(iter++, queue.removeFirst());
		ResultSet resultSet = statement.executeQuery();

		String results = "\t";
		for(int i = 1; i <= 9; i++)
			switch (resultSet.getMetaData().getColumnLabel(i)) {
			case "TITLE":
				results += String.format("%-30s", "TITLE") + "\t";
				break;
			case "YEAR":
				results += String.format("%4s", "YEAR") + "\t";
				break;
			case "LENGTH":
				results += String.format("%6s", "LENGTH") + "\t";
				break;
			case "PG_RATING":
				results += String.format("%9s", "PG_RATING") + "\t";
				break;
			case "GENRE":
				results += String.format("%20s", "GENRE") + "\t";
				break;
			case "ACTOR":
				results += String.format("%20s", "ACTOR") + "\t";
				break;
			case "ACTOR_ROLE":
				results += String.format("%20s", "ACTOR_ROLE") + "\t";
				break;
			case "DIRECTOR":
				results += String.format("%20s", "DIRECTOR") + "\t";
				break;
			case "WRITER":
				results += String.format("%20s", "WRITER") + "\t";
				break;
			}
		results += "\n\t____________________________________________________________________________________________________________________________________________________________________________________\n";
		int count = 1;
		while(resultSet.next()) {
			results += count++ + ".\t";
			for(int i = 1; i <= 9; i++)
				switch (resultSet.getMetaData().getColumnLabel(i)) {
				case "TITLE":
					results += String.format("%-30s", resultSet.getString(i)) + "\t";
					break;
				case "YEAR":
					results += String.format("%4s", resultSet.getString(i)) + "\t";
					break;
				case "LENGTH":
					results += String.format("%6s", resultSet.getString(i)) + "\t";
					break;
				case "PG_RATING":
					results += String.format("%9s", resultSet.getString(i)) + "\t";
					break;
				default:
					results += String.format("%20s", resultSet.getString(i)) + "\t";
					break;
				}
			results += "\n";
		}

		resultSet.close();
		statement.close();
		return results;
	}





	//query showing using parameters
	//return a formatted string of the result of the query
	public String searchShow(String movie) throws SQLException {
		String query = "SELECT title, showing_time showing, rNum room, price FROM showing";
		if(movie != null)
			query += " WHERE title = ?";
		PreparedStatement statement = conn.prepareStatement(query);
		if(movie != null)
			statement.setString(1, movie);
		ResultSet resultSet = statement.executeQuery();

		String results = "\t";
		for(int i = 1; i <= 4; i++)
			switch (resultSet.getMetaData().getColumnLabel(i)) {
			case "TITLE":
				results += String.format("%-30s", "TITLE") + "\t";
				break;
			case "SHOWING":
				results += String.format("%19s", "SHOWING") + "\t";
				break;
			case "ROOM":
				results += String.format("%4s", "ROOM") + "\t";
				break;
			case "PRICE":
				results += String.format("%6s", "PRICE") + "\t";
				break;
			}
		results += "\n\t______________________________________________________________________\n";
		int count = 1;
		while(resultSet.next()) {
			results += count++ + ".\t";
			for(int i = 1; i <= 4; i++)
				switch (resultSet.getMetaData().getColumnLabel(i)) {
				case "TITLE":
					results += String.format("%-30s", resultSet.getString(i)) + "\t";
					break;
				case "SHOWING":
					SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd");
					SimpleDateFormat time = new SimpleDateFormat(" hh:mm aa");
					results += String.format("%19s", date.format(resultSet.getDate(i)) + time.format(resultSet.getTime(i))) + "\t";
					break;
				case "ROOM":
					results += String.format(" %03d", resultSet.getInt(i)) + "\t";
					break;
				case "PRICE":
					DecimalFormat df = new DecimalFormat("$0.00");
					results += String.format("%1$6s", df.format(resultSet.getDouble(i))) + "\t";
					break;
				}
			results += "\n";
		}

		resultSet.close();
		statement.close();
		return results;
	}





	//add movie if only movie specified
	//add actor, director, and/or writer if no movie specified
	//add movie/actor, movie/director, and/or movie/writer relationships otherwise
	//use input dialogs to ask for additional information from the user as needed
	//call addMovie(), addActor(), addDirector(), addWriter(), addActs(), addDirects(), and addWrites() helper methods
	public void addInfo(String title, String actor, String director, String writer) throws Exception {
		int year = 0;
		if(title != null)
			try {
				year = Integer.parseInt(JOptionPane.showInputDialog("What year was " + title + " made in?"));
			} catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Enter a whole number.");
			}
		if(title != null && actor != null)
			addActs(title, year, actor, JOptionPane.showInputDialog("Enter the date of birth for actor " + actor + " in YYYY/MM/DD form."), JOptionPane.showInputDialog("What is " + actor + "'s role in " + title + "?"));
		else if(actor != null)
			addActor(actor, JOptionPane.showInputDialog("Enter the date of birth for actor " + actor + " in YYYY/MM/DD form."));
		if(title != null && director != null)
			addDirects(title, year, director, JOptionPane.showInputDialog("Enter the date of birth for director " + director + " in YYYY/MM/DD form."));
		else if(director != null)
			addDirector(director, JOptionPane.showInputDialog("Enter the date of birth for director " + director + " in YYYY/MM/DD form."));
		if(title != null && writer != null)
			addWrites(title, year, writer, JOptionPane.showInputDialog("Enter the date of birth for writer " + writer + " in YYYY/MM/DD form."));
		else if(writer != null)
			addWriter(writer, JOptionPane.showInputDialog("Enter the date of birth for writer " + writer + " in YYYY/MM/DD form."));
		if(title != null && actor == null && director == null && writer == null)
			addMovie(title, year);
	}

	//query movie to check if movie does not already exist
	//insert movie into movie if it does not exist, and also insert genre into movie_genre
	//use input dialogs to ask for additional information from the user as needed
	private void addMovie(String title, int year) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT title FROM movie WHERE title = ? AND movie_year = ?");
		statement.setString(1, title);
		statement.setInt(2, year);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, title + " already exists.");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("INSERT INTO movie values (?,?,?,?)");
		statement.setString(1, title);
		statement.setInt(2, year);
		try {
			statement.setInt(3, Integer.parseInt(JOptionPane.showInputDialog("How long is " + title + " in minutes?")));
		} catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Enter a whole number.");
		}
		statement.setString(4, JOptionPane.showInputDialog("What is the pg_rating for " + title + "?").toUpperCase());
		statement.executeUpdate();
		statement.close();
		statement = conn.prepareStatement("INSERT INTO movie_genre VALUES (?,?,?)");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, JOptionPane.showInputDialog("What is the genre for " + title + "?"));
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, title + " added.");
		resultSet.close();
		statement.close();
	}

	//query person to check if actor does not already exist
	//update person if he/she exists but is not already an actor
	//insert new person as actor otherwise
	//use input dialogs to ask for additional information from the user as needed
	private void addActor(String actor, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT actor FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, actor);
		statement.setString(2, dob);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			if(resultSet.getInt(1) != 1) {
				PreparedStatement statement2 = conn.prepareStatement("UPDATE person SET actor = 1 WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
				statement2.setString(1, actor);
				statement2.setString(2, dob);
				statement2.executeUpdate();
				statement2.close();
				JOptionPane.showMessageDialog(null, actor + " updated to actor.");
			}
			else
				JOptionPane.showMessageDialog(null, actor + " already exists as actor.");
		}
		else {
			PreparedStatement statement2 = conn.prepareStatement("INSERT INTO person VALUES (?,to_date(?,'YYYY/MM/DD'),?,?,?,?)");
			statement2.setString(1, actor);
			statement2.setString(2, dob);
			statement2.setString(3, JOptionPane.showInputDialog("Enter M if " + actor + " is male; F if female.").toUpperCase());
			statement2.setInt(4, 1);
			statement2.setInt(5, 0);
			statement2.setInt(6, 0);
			statement2.executeUpdate();
			statement2.close();
			JOptionPane.showMessageDialog(null, actor + " added as actor.");
		}
		resultSet.close();
		statement.close();
	}

	//query person to check if director does not already exist
	//update person if he/she exists but is not already a director
	//insert new person as director otherwise
	//use input dialogs to ask for additional information from the user as needed
	private void addDirector(String director, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT director FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, director);
		statement.setString(2, dob);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			if(resultSet.getInt(1) != 1) {
				PreparedStatement statement2 = conn.prepareStatement("UPDATE person SET director = 1 WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
				statement2.setString(1, director);
				statement2.setString(2, dob);
				statement2.executeUpdate();
				statement2.close();
				JOptionPane.showMessageDialog(null, director + " updated to director.");
			}
			else
				JOptionPane.showMessageDialog(null, director + " already exists as director.");
		}
		else {
			PreparedStatement statement2 = conn.prepareStatement("INSERT INTO person VALUES (?,to_date(?,'YYYY/MM/DD'),?,?,?,?)");
			statement2.setString(1, director);
			statement2.setString(2, dob);
			statement2.setString(3, JOptionPane.showInputDialog("Enter M if " + director + " is male; F if female.").toUpperCase());
			statement2.setInt(4, 0);
			statement2.setInt(5, 1);
			statement2.setInt(6, 0);
			statement2.executeUpdate();
			statement2.close();
			JOptionPane.showMessageDialog(null, director + " added as director.");
		}
		resultSet.close();
		statement.close();
	}

	//query person to check if writer does not already exist
	//update person if he/she exists but is not already a writer
	//insert new person as writer otherwise
	//use input dialogs to ask for additional information from the user as needed
	private void addWriter(String writer, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT writer FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, writer);
		statement.setString(2, dob);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			if(resultSet.getInt(1) != 1) {
				PreparedStatement statement2 = conn.prepareStatement("UPDATE person SET writer = 1 WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
				statement2.setString(1, writer);
				statement2.setString(2, dob);
				statement2.executeUpdate();
				statement2.close();
				JOptionPane.showMessageDialog(null, writer + " updated to writer.");
			}
			else
				JOptionPane.showMessageDialog(null, writer + " already exists as writer.");
		}
		else {
			PreparedStatement statement2 = conn.prepareStatement("INSERT INTO person VALUES (?,to_date(?,'YYYY/MM/DD'),?,?,?,?)");
			statement2.setString(1, writer);
			statement2.setString(2, dob);
			statement2.setString(3, JOptionPane.showInputDialog("Enter M if " + writer + " is male; F if female.").toUpperCase());
			statement2.setInt(4, 0);
			statement2.setInt(5, 0);
			statement2.setInt(6, 1);
			statement2.executeUpdate();
			statement2.close();
			JOptionPane.showMessageDialog(null, writer + " added as writer.");
		}
		resultSet.close();
		statement.close();
	}

	//query acts to check if movie/actor relationship does not already exist
	//insert movie/actor combo if it does not exist
	//update person if he/she exists but is not already an actor
	private void addActs(String title, int year, String actor, String dob, String role) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT pName FROM acts WHERE title = ? AND movie_year = ? AND pName = ? AND dob = to_date(?,'YYYY/MM/DD') AND movie_role = ?");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, actor);
		statement.setString(4, dob);
		statement.setString(5, role);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, actor + " already exists for " + title + " as actor.");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("INSERT INTO acts VALUES (?,?,?,to_date(?,'YYYY/MM/DD'),?)");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, actor);
		statement.setString(4, dob);
		statement.setString(5, role);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, actor + " added to " + title + " as actor.");
		statement.close();
		statement = conn.prepareStatement("SELECT actor FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, actor);
		statement.setString(2, dob);
		resultSet = statement.executeQuery();
		if(resultSet.next() && resultSet.getInt(1) != 1) {
			PreparedStatement statement2 = conn.prepareStatement("UPDATE person SET actor = 1 WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
			statement2.setString(1, actor);
			statement2.setString(2, dob);
			statement2.executeUpdate();
			statement2.close();
			JOptionPane.showMessageDialog(null, actor + " updated to actor.");
		}
		resultSet.close();
		statement.close();
	}

	//query directs to check if movie/director relationship does not already exist
	//insert movie/director combo if it does not exist
	//update person if he/she exists but is not already a director
	private void addDirects(String title, int year, String director, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT pName FROM directs WHERE title = ? AND movie_year = ? AND pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, director);
		statement.setString(4, dob);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, director + " already exists for " + title + " as director.");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("INSERT INTO directs VALUES (?,?,?,to_date(?,'YYYY/MM/DD'))");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, director);
		statement.setString(4, dob);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, director + " added to " + title + " as director.");
		statement.close();
		statement = conn.prepareStatement("SELECT director FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, director);
		statement.setString(2, dob);
		resultSet = statement.executeQuery();
		if(resultSet.next() && resultSet.getInt(1) != 1) {
			PreparedStatement statement2 = conn.prepareStatement("UPDATE person SET director = 1 WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
			statement2.setString(1, director);
			statement2.setString(2, dob);
			statement2.executeUpdate();
			statement2.close();
			JOptionPane.showMessageDialog(null, director + " updated to director.");
		}
		resultSet.close();
		statement.close();
	}

	//query writes to check if movie/writer relationship does not already exist
	//insert movie/writer combo if it does not exist
	//update person if he/she exists but is not already a writer
	private void addWrites(String title, int year, String writer, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT pName FROM writes WHERE title = ? AND movie_year = ? AND pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, writer);
		statement.setString(4, dob);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, writer + " already exists for " + title + " as writer.");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("INSERT INTO writes VALUES (?,?,?,to_date(?,'YYYY/MM/DD'))");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, writer);
		statement.setString(4, dob);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, writer + " added to " + title + " as writer.");
		statement.close();
		statement = conn.prepareStatement("SELECT writer FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, writer);
		statement.setString(2, dob);
		resultSet = statement.executeQuery();
		if(resultSet.next() && resultSet.getInt(1) != 1) {
			PreparedStatement statement2 = conn.prepareStatement("UPDATE person SET writer = 1 WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
			statement2.setString(1, writer);
			statement2.setString(2, dob);
			statement2.executeUpdate();
			statement2.close();
			JOptionPane.showMessageDialog(null, writer + " updated to writer.");
		}
		resultSet.close();
		statement.close();
	}





	//delete movie if only movie specified
	//delete actor, director, and/or writer if no movie specified
	//delete movie/actor, movie/director, and/or movie/writer relationships otherwise
	//use input dialogs to ask for additional information from the user as needed
	//call removeMovie(), removeActor(), removeDirector(), removeWriter(), removeActs(), removeDirects(), and removeWrites() helper methods
	public void removeInfo(String title, String actor, String director, String writer) throws HeadlessException, SQLException {
		int year = 0;
		if(title != null)
			try {
				year = Integer.parseInt(JOptionPane.showInputDialog("What year was " + title + " made in?"));
			} catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Enter a whole number.");
			}
		if(title != null && actor != null)
			removeActs(title, year, actor, JOptionPane.showInputDialog("Enter the date of birth for actor " + actor + " in YYYY/MM/DD form."), JOptionPane.showInputDialog("What is " + actor + "'s role in " + title + "?"));
		else if(actor != null)
			removeActor(actor, JOptionPane.showInputDialog("Enter the date of birth for actor " + actor + " in YYYY/MM/DD form."));
		if(title != null && director != null)
			removeDirects(title, year, director, JOptionPane.showInputDialog("Enter the date of birth for director " + director + " in YYYY/MM/DD form."));
		else if(director != null)
			removeDirector(director, JOptionPane.showInputDialog("Enter the date of birth for director " + director + " in YYYY/MM/DD form."));
		if(title != null && writer != null)
			removeWrites(title, year, writer, JOptionPane.showInputDialog("Enter the date of birth for writer " + writer + " in YYYY/MM/DD form."));
		else if(writer != null)
			removeWriter(writer, JOptionPane.showInputDialog("Enter the date of birth for writer " + writer + " in YYYY/MM/DD form."));
		if(title != null && actor == null && director == null && writer == null)
			removeMovie(title, year);
	}

	//query movie to check if movie exists
	//delete movie if it exists
	private void removeMovie(String title, int year) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT title FROM movie WHERE title = ? AND movie_year = ?");
		statement.setString(1, title);
		statement.setInt(2, year);
		ResultSet resultSet = statement.executeQuery();
		if(!resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, title + " does not exist.");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("DELETE FROM movie WHERE title = ? AND movie_year = ?");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, title + " removed.");
	}

	//query person to check if actor exists
	//delete actor if he/she exists
	private void removeActor(String actor, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT pName FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD') AND actor = 1");
		statement.setString(1, actor);
		statement.setString(2, dob);
		ResultSet resultSet = statement.executeQuery();
		if(!resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, actor + " does not exist as an actor.");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("DELETE FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD') AND actor = 1");
		statement.setString(1, actor);
		statement.setString(2, dob);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, actor + " removed.");
	}

	//query person to check if director exists
	//delete director if he/she exists
	private void removeDirector(String director, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT pName FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD') AND director = 1");
		statement.setString(1, director);
		statement.setString(2, dob);
		ResultSet resultSet = statement.executeQuery();
		if(!resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, director + " does not exist as a director.");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("DELETE FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD') AND director = 1");
		statement.setString(1, director);
		statement.setString(2, dob);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, director + " removed.");
	}

	//query person to check if writer exists
	//delete writer if he/she exists
	private void removeWriter(String writer, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT pName FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD') AND writer = 1");
		statement.setString(1, writer);
		statement.setString(2, dob);
		ResultSet resultSet = statement.executeQuery();
		if(!resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, writer + " does not exist as a writer.");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("DELETE FROM person WHERE pName = ? AND dob = to_date(?,'YYYY/MM/DD') AND writer = 1");
		statement.setString(1, writer);
		statement.setString(2, dob);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, writer + " removed.");
	}

	//query acts to check if movie/actor relationship exists
	//delete movie/actor combo if it exists
	private void removeActs(String title, int year, String actor, String dob, String role) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT title FROM acts WHERE title = ? AND movie_year = ? AND pName = ? AND dob = to_date(?,'YYYY/MM/DD') AND movie_role = ?");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, actor);
		statement.setString(4, dob);
		statement.setString(5, role);
		ResultSet resultSet = statement.executeQuery();
		if(!resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, actor + " is not an actor for " + title + ".");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("DELETE FROM acts WHERE title = ? AND movie_year = ? AND pName = ? AND dob = to_date(?,'YYYY/MM/DD') AND movie_role = ?");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, actor);
		statement.setString(4, dob);
		statement.setString(5, role);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, actor + " removed from " + title + " as actor.");
	}

	//query directs to check if movie/director relationship exists
	//delete movie/director combo if it exists
	private void removeDirects(String title, int year, String director, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT title FROM directs WHERE title = ? AND movie_year = ? AND pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, director);
		statement.setString(4, dob);
		ResultSet resultSet = statement.executeQuery();
		if(!resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, director + " is not a director for " + title + ".");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("DELETE FROM directs WHERE title = ? AND movie_year = ? AND pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, director);
		statement.setString(4, dob);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, director + " removed from " + title + " as director.");
	}

	//query writes to check if movie/writer relationship exists
	//delete movie/writer combo if it exists
	private void removeWrites(String title, int year, String writer, String dob) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT title FROM writes WHERE title = ? AND movie_year = ? AND pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, writer);
		statement.setString(4, dob);
		ResultSet resultSet = statement.executeQuery();
		if(!resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, writer + " is not a writer for " + title + ".");
			return;
		}
		resultSet.close();
		statement.close();
		statement = conn.prepareStatement("DELETE FROM writes WHERE title = ? AND movie_year = ? AND pName = ? AND dob = to_date(?,'YYYY/MM/DD')");
		statement.setString(1, title);
		statement.setInt(2, year);
		statement.setString(3, writer);
		statement.setString(4, dob);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, writer + " removed from " + title + " as writer.");
	}





	//query ticket to check if ticket info does not exist
	//insert ticket info if it does not exist
	public void buyTicket(String time, int room, String username) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("SELECT showing_time FROM ticket WHERE showing_time = to_date(?,'YYYY/MM/DD HH12:MI AM') AND rNum = ? AND username = ?");
		statement.setString(1, time);
		statement.setInt(2, room);
		statement.setString(3, username);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			resultSet.close();
			statement.close();
			JOptionPane.showMessageDialog(null, "Ticket already owned by " + username + ".");
			return;
		}
		statement.close();
		statement = conn.prepareStatement("INSERT INTO ticket VALUES (?,to_date(?,'YYYY/MM/DD HH12:MI AM'),?)");
		statement.setInt(1, room);
		statement.setString(2, time);
		statement.setString(3, username);
		statement.executeUpdate();
		statement.close();
		JOptionPane.showMessageDialog(null, "Ticket bought by " + username + ".");
		statement.close();
	}
}
